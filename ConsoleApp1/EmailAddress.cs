﻿namespace ConsoleApp1
{
    public class EmailAddress
    {
        public string LocalPart { get; set; }
        public string Domain { get; set; }
        public EmailAddress(string localPart, string domain)
        {
            LocalPart = localPart;
            Domain = domain;
        }
        public string GetEmailAddressString()
        {
            return LocalPart + "@" + Domain;
        }
    }
}